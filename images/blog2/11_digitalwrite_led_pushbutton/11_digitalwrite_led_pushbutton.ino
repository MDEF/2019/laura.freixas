/* # When you push the digital button, the Led 13 on the board will turn on. Otherwise,the led turns off.
*/
int ledPin = 3;                // choose the pin 3 for the LED
int inputPin = 12;               // Connect sensor to input pin 12 
void setup() {
  pinMode(ledPin, OUTPUT);      // declare LED as output
  pinMode(inputPin, INPUT);     // declare pushbutton as input
}
void loop(){
  int val = digitalRead(inputPin);  // read input value
  if (val == HIGH) {            // check if the input is HIGH
    digitalWrite(ledPin, LOW);  // turn LED OFF
  } else {
    digitalWrite(ledPin, HIGH); // turn LED ON
  }
}
